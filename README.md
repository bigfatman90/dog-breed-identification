# Dog Breed Identification

## Name
Dog Breed Identification.

## Description
The project is design to help people to identify the dog's breed with uploading a photo of a dog to a service ( might be a telegram boot or other service, it's TBD) and the service will return the breed of the dog

## Installation
All work (including development) is done in the docker container. To build an image for the project you can run ```docker build -t <image_name> .``` to build an image and then ```docker run -it <image_name>``` to run a docker in a interactive way.

## Usage
Here will be an information regarding the way of using the project

## Repository Management Methodology

We adhere to specific methodologies for managing development and collaboration in our repository. This includes the following aspects:

### Branching and Branch Management

We use a custom branching model to manage branch-based development. The primary branching rules are as follows:

- `master`: The main branch containing stable product releases.
- `experiment_#`: The experiment branch where we conduct experiments. Not all the branches will be merged to master, but we still need to store them since the results of all experiments are worthy.
Since the model is custom, it might be adjusted or switched to a new one.

## Contributing
The project in the current status is not anticipating to be contributed from other people, except the author

## Authors and acknowledgment
Maksim Barylau

## License
MIT

## Project status
In progress
