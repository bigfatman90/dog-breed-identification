# Contributing to Dog-breed-identification

Thank you for your interest in contributing to Dog-breed-identification! This project is currently managed by the author and is intended for educational purposes only. As such, contributions from external contributors are not expected or solicited at this time.

## About the Project

Dog-breed-identification is an educational initiative created by Maksim Barylau for the purpose of acquiring MLops skills . It serves as a learning resource for MLops 3.0 course powered by ODS.

## Project Management

This project is currently managed and maintained solely by Maksim Barylau. As the project author, I am responsible for maintaining the project's codebase, documentation, and overall direction.

## Contributions

While contributions from external contributors are appreciated, this project is primarily designed for educational purposes and does not currently include a collaborative development model. Therefore, contributions such as bug fixes, feature requests, or documentation improvements are not expected or actively encouraged at this time.

## Using Linters

Linters are tools that help ensure code quality and consistency. In our project, we use the following linters:
- [RUFF] (https://docs.astral.sh/ruff/) for Python

## Setting Up Linters

1. **Install Dependencies**: Make sure you have the necessary dependencies installed. You can usually find these in the project's `requirements.txt`, `environment.yaml`, `pyproject.toml` or similar files.
   
2. **Configuration**: Each linter may require its own configuration file. The files is usually named `pyproject.toml`. You may need to customize the file according to your project's needs.

3. **Integration with IDEs/Editors**: To streamline the linting process, consider integrating the linters with your preferred IDE or text editor. Many popular editors have plugins/extensions available for this purpose.

## Getting Help

If you have any questions, feedback, or suggestions related to Dog-breed-identification, please feel free to reach out to Maksim Barylau directly via bigfatman90@gmail.com.

## License

This project is licensed under the MIT License. By contributing to Dog-breed-identification, you agree to license your contributions under the same terms.
