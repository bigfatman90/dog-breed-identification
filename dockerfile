FROM  mambaorg/micromamba:1.5.8 

USER root
WORKDIR /dog-breed-identification
COPY env.yml /dog-breed-identification/env.yml

RUN apt-get upgrade
RUN apt-get update
RUN apt-get install wget -y
# RUN wget https://github.com/quarto-dev/quarto-cli/releases/download/v1.4.553/quarto-1.4.553-linux-arm64.deb
RUN wget https://github.com/quarto-dev/quarto-cli/releases/download/v1.4.553/quarto-1.4.553-linux-amd64.deb

# RUN dpkg -i quarto-1.4.553-linux-arm64.deb
RUN dpkg -i quarto-1.4.553-linux-amd64.deb

# RUN rm quarto-1.4.553-linux-arm64.deb
RUN rm quarto-1.4.553-linux-amd64.deb

RUN micromamba create -f /dog-breed-identification/env.yml

COPY pyproject.toml /dog-breed-identification/pyproject.toml
COPY pdm.lock /dog-breed-identification/pdm.lock

COPY street_tree_census/EDA.ipynb  /dog-breed-identification/street_tree_census/EDA.ipynb

RUN micromamba run -n dog_dev pdm install